#! /usr/bin/env bash
set -euxo pipefail


# Let the DB start
echo "postgres waiting"

while ! nc -z db 5432; do
    sleep 1
    echo "sleeping waiting db"
done

echo "postgres wait finished"
if [[ "$RUN_MIGRATIONS" = true ]]; then
    if [[ -d /app/migrations ]]; then
        echo "alembic magic"
        PYTHONPATH=. alembic upgrade head
        echo "alembic finished"
    fi
fi

# Run custom Python script before starting
#python manage.py

echo "prestart euri10_fastapi_base finished".