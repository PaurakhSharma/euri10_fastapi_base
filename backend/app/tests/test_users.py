import asyncio
import json
import logging
import math
import random
from datetime import datetime

import pytest
from faker import Faker
from sqlalchemy import desc

from main import app
from models.users import UserIn, UserOut, UserPatch, users
from settings import database
from utils.other import status_for
from utils.password import get_password_hash, verify_password

fake = Faker()

logger = logging.getLogger(__name__)


# sync databases utility
def sync(coroutine):
    event_loop = None
    try:
        event_loop = asyncio.get_event_loop()
    except RuntimeError:
        event_loop = asyncio.new_event_loop()
        asyncio.set_event_loop(event_loop)
    return event_loop.run_until_complete(coroutine)


def test_create_user(client):
    url = app.url_path_for("users_create")
    sc = status_for(app.routes, "users_create")
    userin = UserIn(
        name=fake.name(),
        email=fake.email(),
        password=get_password_hash(fake.password()),
    )
    response = client.post(url, json=userin.dict())
    assert response.status_code == sc
    expect_out = UserOut(
        **userin.dict(),
        time_created=datetime.strptime(
            response.json().get("time_created"), "%Y-%m-%dT%H:%M:%S.%f%z"
        ),
        time_updated=None,
    ).json()
    assert response.json() == json.loads(expect_out)
    response = client.post(url, json=userin.dict())
    assert response.status_code == 409
    assert response.json() == {"detail": "Duplicate email or username"}

    userin = UserIn(
        name=fake.name(),
        email=fake.email(),
        password=get_password_hash(fake.password()),
    )
    additional = userin.dict().update({"key": "value"})
    response = client.post(url, json=additional)
    assert response.status_code == 422


def test_users_read_byname(client, register_one_user):
    # by name
    url = app.url_path_for("users_read", email_or_name=register_one_user.name)
    response = client.get(url)
    assert response.status_code == 200
    expect_out = UserOut(
        **register_one_user.dict(),
        time_created=datetime.strptime(
            response.json().get("time_created"), "%Y-%m-%dT%H:%M:%S.%f%z"
        ),
        time_updated=None,
    ).json()
    assert response.json() == json.loads(expect_out)


def test_users_read_byname_wrong(client, register_one_user):
    # by name
    _name = fake.name()
    assert _name != register_one_user.name
    url = app.url_path_for("users_read", email_or_name=_name)
    response = client.get(url)
    assert response.status_code == 404
    assert response.json() == {
        "detail": f"The user {_name} does not exist in the system."
    }


def test_users_read_byemail(client, register_one_user):
    # # by email
    url = app.url_path_for("users_read", email_or_name=register_one_user.email)
    response = client.get(url)
    assert response.status_code == 200
    expect_out = UserOut(
        **register_one_user.dict(),
        time_created=datetime.strptime(
            response.json().get("time_created"), "%Y-%m-%dT%H:%M:%S.%f%z"
        ),
        time_updated=None,
    ).json()
    assert response.json() == json.loads(expect_out)


def test_users_read_byemail_wrong(client, register_one_user):
    # # by email
    email = fake.email()
    assert email != register_one_user.email
    url = app.url_path_for("users_read", email_or_name=email)
    response = client.get(url)
    assert response.status_code == 404
    assert response.json() == {
        "detail": f"The user {email} does not exist in the system."
    }


def test_users_modify_email(client, register_one_user):
    new_email = fake.email()
    assert new_email != register_one_user.email
    url = app.url_path_for("users_modify", _name=register_one_user.name)
    patch_data = UserPatch(email=new_email)
    response = client.patch(url, json=patch_data.dict(skip_defaults=True))
    assert response.status_code == 204
    assert response.content == b""
    url = app.url_path_for("users_read", email_or_name=register_one_user.name)
    response = client.get(url)
    assert response.status_code == 200
    assert response.json().get("email") == new_email


def test_users_modify_password(client, register_one_user):
    new_password = fake.password()
    url = app.url_path_for("users_modify", _name=register_one_user.name)
    patch_data = UserPatch(password=new_password)
    response = client.patch(url, json=patch_data.dict(skip_defaults=True))
    assert response.status_code == 204
    assert response.content == b""
    # check new pass hash corresponds directly in db
    query = users.select().where(users.c.name == register_one_user.name)
    result = sync(database.fetch_one(query))
    assert verify_password(new_password, result["password"])


def test_users_modify_email_with_existing_email(client, register_two_users):

    user1, user2 = register_two_users

    url = app.url_path_for("users_modify", _name=user1.name)
    patch_data = UserPatch(email=user2.email)
    response = client.patch(url, json=patch_data.dict(skip_defaults=True))
    assert response.status_code == 409
    assert response.json() == {"detail": "Conflit updating"}

    # check old email still ok
    url = app.url_path_for("users_read", email_or_name=user1.name)
    response = client.get(url)
    assert response.status_code == 200
    assert response.json().get("email") == user1.email


def test_users_modify_name_with_existing_name(client, register_two_users):

    user1, user2 = register_two_users

    url = app.url_path_for("users_modify", _name=user1.name)
    patch_data = UserPatch(name=user2.name)
    response = client.patch(url, json=patch_data.dict(skip_defaults=True))
    assert response.status_code == 409
    assert response.json() == {"detail": "Conflit updating"}

    # check old email still ok
    url = app.url_path_for("users_read", email_or_name=user1.name)
    response = client.get(url)
    assert response.status_code == 200
    assert response.json().get("name") == user1.name


def test_users_modify_with_empty(client, register_one_user):
    url = app.url_path_for("users_modify", _name=register_one_user.name)
    patch_data = UserPatch()
    response = client.patch(url, json=patch_data.dict(skip_defaults=True))
    assert response.status_code == 204
    assert response.content == b""

    # check nothing changed
    url = app.url_path_for("users_read", email_or_name=register_one_user.name)
    response = client.get(url)
    assert response.status_code == 200
    assert response.json().get("name") == register_one_user.name
    assert response.json().get("email") == register_one_user.email


data_page_size = [(30, 12), (10, 100), (5, 5), (100, 22)]


@pytest.mark.parametrize("users_count, page_size", data_page_size)
def test_users_pagination(client, users_count, page_size):
    create_users = []

    for _ in range(users_count):
        userin = UserIn(
            name=f"{fake.user_name()}_{random.randint(0,10)}",
            email=f"{fake.email()}",
            password=fake.password(),  # we dont hash password for speed
        )
        create_users.append(userin)
    query = users.insert().returning(users.c.id)
    sync(database.execute_many(query, [u.dict() for u in create_users]))

    # created_uuid_list contains the whole created users
    query = users.select().order_by(desc(users.c.id))
    created_uuid_list = sync(database.fetch_all(query))
    url = app.url_path_for("users_list")

    # get next pages
    loops = math.ceil(users_count / page_size)
    npt = None
    for l in range(loops):
        logger.debug(f"Loop {l+1} / {loops}")
        url = app.url_path_for("users_list")
        response = client.get(
            url, params={"page_size": page_size, "next_page_token": npt}
        )
        assert response.status_code == 200
        expected_out_names = [
            UserOut(**r).dict().get("name") for r in created_uuid_list
        ][l * page_size : (l + 1) * page_size]
        out_names = [r.get("name") for r in response.json().get("users")]
        expected_out_emails = [
            UserOut(**r).dict().get("email") for r in created_uuid_list
        ][l * page_size : (l + 1) * page_size]
        out_emails = [r.get("email") for r in response.json().get("users")]
        assert expected_out_names == out_names
        assert expected_out_emails == out_emails
        logger.debug(
            f"len(expected_out_names) slice = {len(expected_out_names)}  /  {len(out_names)}"
        )
        logger.debug(
            f"len(expected_out_emails) slice = {len(expected_out_emails)}  /  {len(out_emails)}"
        )
        npt = response.json().get("next_page_token")
